﻿using System;
using System.Data;
using System.Web.UI;

public partial class Default : Page
{
    private Customer _selectedCustomer;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) this.ddlCustomer.DataBind();
        this._selectedCustomer = this.GetSelectedCustomer();
        this.lblCustomerID.Text = this._selectedCustomer.CustomerId;
        this.lblAddress.Text = this._selectedCustomer.Address;
        this.lblCity.Text = this._selectedCustomer.City;
        this.lblState.Text = this._selectedCustomer.State;
        this.lblZipCode.Text = this._selectedCustomer.ZipCode;
        this.lblPhone.Text = this._selectedCustomer.PhoneNumber;
        this.lblEmail.Text = this._selectedCustomer.Email;
    }

    private Customer GetSelectedCustomer()
    {
        var customerTable = (DataView)this.SqlDataSource1.Select(DataSourceSelectArguments.Empty);
        var customer = new Customer();

        if (customerTable == null)
        {
            return customer;
        }
        customerTable.RowFilter = string.Format("CustomerID = '{0}'", this.ddlCustomer.SelectedValue);
        var row = customerTable[0];


        customer.CustomerId = row["CustomerID"].ToString();
        customer.FullName = row["Name"].ToString();
        customer.Address = row["Address"].ToString();
        customer.City = row["City"].ToString();
        customer.State = row["State"].ToString();
        customer.ZipCode = row["ZipCode"].ToString();
        customer.PhoneNumber = row["Phone"].ToString();
        customer.Email = row["Email"].ToString();

        return customer;
    }

    protected void btnViewContacts_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContactList.aspx");
    }

    protected void btnAddContact_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }

        var customerList = CustomerList.GetCustomers();
        var customer = customerList[this._selectedCustomer.CustomerId];

        if (customer == null)
        {
            customerList.AddItem(this._selectedCustomer);
        }
        else
        {
           this.lblErrorMessage.Text = "Customer is already in the list";
        }
    }
}