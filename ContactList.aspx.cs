﻿using System;
using System.Web.UI;

public partial class ContactEdit : Page
{

    private CustomerList _customerList;

    protected void Page_Load(object sender, EventArgs e)
    {
         this._customerList = CustomerList.GetCustomers();

        if (!IsPostBack)
        {
            this.DisplayCustomers();
        }
    }

    private void DisplayCustomers()
    {
        this.lbCustomerList.Items.Clear();

       
        for (var i = 0; i < this._customerList.Count; i++)
        {
            var customer = this._customerList[i];
           this.lbCustomerList.Items.Add(customer.Display());
        }
    }

    protected void btnSelectAdditonalCustomers_Click(object sender, EventArgs e)
    {
        Response.Redirect("CustomerList.aspx");
    }

    protected void btnRemoveCustomer_Click(object sender, EventArgs e)
    {
        if (this._customerList.Count <= 0)
        {
            return;
        }
        if (this.lbCustomerList.SelectedIndex > -1)
        {
            this._customerList.RemoveAt(this.lbCustomerList.SelectedIndex);
            this.DisplayCart();
        }
        else
        {
            this.lblMessage.Text = "Please select an item to remove";
        }
    }

    private void DisplayCart()
    {
        this.lbCustomerList.Items.Clear();

        for (var i = 0; i < this._customerList.Count; i++)
        {
            this.lbCustomerList.Items.Add(this._customerList[i].Display());
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        if (this._customerList.Count <= 0)
        {
            return;
        }
        this._customerList.Clear();
        this.lbCustomerList.Items.Clear();
    }
}