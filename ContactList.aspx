﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContactList.aspx.cs" Inherits="ContactEdit" MasterPageFile="DigitalManger.master" ClientIDMode="Static" Title="Sports Man. Inc : Contacts" %>

<asp:Content runat="server" ContentPlaceHolderID="headplaceHolder">
    <link href="Styles/Home.css" rel="stylesheet" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="headerPlaceHolder">
    <h2>Contact List</h2>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="formPlaceholder">
    <form id="form1" runat="server">
         <label id="contactListHeader">Contacts: </label>

        <asp:ListBox ID="lbCustomerList" runat="server"></asp:ListBox>

        <asp:Button ID="btnRemoveCustomer" CssClass="sideButton" runat="server" Text="Remove Customer" OnClick="btnRemoveCustomer_Click" />
        <asp:Button ID="btnClear" CssClass="sideButton" runat="server" Text="Clear List" OnClick="btnClear_Click" />

        <asp:Button ID="btnSelectAdditonalCustomers" CssClass="button" runat="server" Text="Select Additional Customers" OnClick="btnSelectAdditonalCustomers_Click" />
    
        <asp:Label ID="lblMessage" runat="server" CssClass="error" Enabled="False"></asp:Label>
    </form>
</asp:Content>
        