﻿using System;
using System.Diagnostics;

/// <summary>
/// Represents a description object.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// Febuary 5, 2015 | Spring
/// </version>
public class Description
{
    /// <summary>
    /// The _customer identifier
    /// </summary>
    private String _customerId;
    /// <summary>
    /// The _feedback identifier
    /// </summary>
    private String _feedbackId;
    /// <summary>
    /// The _service time
    /// </summary>
    private String _serviceTime;
    /// <summary>
    /// The _efficiency
    /// </summary>
    private String _efficiency;
    /// <summary>
    /// The _resolution
    /// </summary>
    private String _resolution;
    /// <summary>
    /// The _comments
    /// </summary>
    private String _comments;

    /// <summary>
    /// The _contact method
    /// </summary>
    private String _contactMethod;

    /// <summary>
    /// Gets or sets the customer identifier.
    /// </summary>
    /// <value>
    /// The customer identifier.
    /// </value>
    public String CustomerId
    {
        get { return this._customerId; }
        set
        {
            Trace.Assert(value != null, "CustomerId must not be null.");
            this._customerId = value;
        }
    }

    /// <summary>
    /// Gets or sets the feedback identifier.
    /// </summary>
    /// <value>
    /// The feedback identifier.
    /// </value>
    public String FeedbackId
    {
        get { return this._feedbackId; }
        set
        {
            Trace.Assert(value != null, "FeedbackId must not be null.");
            this._feedbackId = value;
        }
    }

    /// <summary>
    /// Gets or sets the service time.
    /// </summary>
    /// <value>
    /// The service time.
    /// </value>
    public String ServiceTime
    {
        get { return this._serviceTime; }
        set
        {
            Trace.Assert(value != null, "ServiceTime must not be null.");
            this._serviceTime = value;
        }
    }

    /// <summary>
    /// Gets or sets the efficiency.
    /// </summary>
    /// <value>
    /// The efficiency.
    /// </value>
    public String Efficiency
    {
        get { return this._efficiency; }
        set
        {
            Trace.Assert(value != null, "Efficiency must not be null.");
            this._efficiency = value;
        }
    }

    /// <summary>
    /// Gets or sets the resolution.
    /// </summary>
    /// <value>
    /// The resolution.
    /// </value>
    public String Resolution
    {
        get { return this._resolution; }
        set
        {
            Trace.Assert(value != null, "Resolution must not be null.");
            this._resolution = value;
        }
    }

    /// <summary>
    /// Gets or sets the comments.
    /// </summary>
    /// <value>
    /// The comments.
    /// </value>
    public String Comments
    {
        get { return this._comments; }
        set
        {
            Trace.Assert(value != null, "Comments must not be null.");
            this._comments = value;
        }
    }

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Description"/> is contact.
    /// </summary>
    /// <value>
    ///   <c>true</c> if contact; otherwise, <c>false</c>.
    /// </value>
    public Boolean Contact { get; set; }

    /// <summary>
    /// Gets or sets the contact method.
    /// </summary>
    /// <value>
    /// The contact method.
    /// </value>
    public String ContactMethod
    {
        get { return this._contactMethod; }
        set
        {
            Trace.Assert(value != null, "Contact must not be null.");
            this._contactMethod = value;
        }
    }
}