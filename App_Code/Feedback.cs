﻿using System;
using System.Diagnostics;

/// <summary>
/// Represents the Feedback from Customers.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// Febuary 5, 2015 | Spring
/// </version>
public class Feedback
{
    /// <summary>
    /// The _feedback identifier
    /// </summary>
    private String _feedbackId;
    /// <summary>
    /// The _customer identifier
    /// </summary>
    private String _customerId;
    /// <summary>
    /// The _software identifier
    /// </summary>
    private String _softwareId;
    /// <summary>
    /// The _support identifier
    /// </summary>
    private String _supportId;
    /// <summary>
    /// The _date opened
    /// </summary>
    private String _dateOpened;
    /// <summary>
    /// The _date closed
    /// </summary>
    private String _dateClosed;
    /// <summary>
    /// The _title
    /// </summary>
    private String _title;
    /// <summary>
    /// The _description
    /// </summary>
    private String _description;

    /// <summary>
    /// Gets or sets the feed back identifier.
    /// </summary>
    /// <value>
    /// The feed back identifier.
    /// </value>
    public String FeedBackId
    {
        get { return this._feedbackId; }
        set
        {
            Trace.Assert(value != null, "FeedBackId must not be null");
            this._feedbackId = value;
        }
    }

    /// <summary>
    /// Gets or sets the customer identifier.
    /// </summary>
    /// <value>
    /// The customer identifier.
    /// </value>
    public String CustomerId
    {
        get { return this._customerId; }
        set
        {
            Trace.Assert(value != null, "CustomerId must not be null");
            this._customerId = value;
        }
    }

    /// <summary>
    /// Gets or sets the software identifier.
    /// </summary>
    /// <value>
    /// The software identifier.
    /// </value>
    public String SoftwareId
    {
        get { return this._softwareId; }
        set
        {
            Trace.Assert(value != null, "SoftwareId must not be null");
            this._softwareId = value;
        }
    }

    /// <summary>
    /// Gets or sets the support identifier.
    /// </summary>
    /// <value>
    /// The support identifier.
    /// </value>
    public String SupportId
    {
        get { return this._supportId; }
        set
        {
            Trace.Assert(value != null, "SupportId must not be null");
            this._supportId = value;
        }
    }

    /// <summary>
    /// Gets or sets the date opened.
    /// </summary>
    /// <value>
    /// The date opened.
    /// </value>
    public String DateOpened
    {
        get { return this._dateOpened; }
        set
        {
            Trace.Assert(value != null, "DateOpened must not be null");
            this._dateOpened = value;
        }
    }

    /// <summary>
    /// Gets or sets the date closed.
    /// </summary>
    /// <value>
    /// The date closed.
    /// </value>
    public String DateClosed
    {
        get { return this._dateClosed; }
        set
        {
            Trace.Assert(value != null, "DateClosed must not be null");
            this._dateClosed = value;
        }
    }

    /// <summary>
    /// Gets or sets the title.
    /// </summary>
    /// <value>
    /// The title.
    /// </value>
    public String Title
    {
        get { return this._title; }
        set
        {
            Trace.Assert(value != null, "Title must not be null");
            this._title = value;
        }
    }

    /// <summary>
    /// Gets or sets the description.
    /// </summary>
    /// <value>
    /// The description.
    /// </value>
    public String Description
    {
        get { return this._description; }
        set
        {
            Trace.Assert(value != null, "Description must not be null");
            this._description = value;
        }
    }


    /// <summary>
    /// Formats the feedback.
    /// </summary>
    /// <returns>
    /// String representation of the feedback.
    /// </returns>
    public String FormatFeedback()
    {
        return "Feedback for software " + this._softwareId + " closed " + this._dateClosed + " (" + this._description + ")";
    }
}