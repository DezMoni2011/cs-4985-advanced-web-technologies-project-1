﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomerList.aspx.cs" Inherits="Default" ClientIDMode="Static" MasterPageFile="DigitalManger.master" Title="Sports Man. Inc : Customer Select" %>


<asp:Content runat="server" ContentPlaceHolderID="headplaceHolder">
      <link href="Styles/Home.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="headerPlaceHolder">
    <h2>Choose a Customer</h2>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="formPlaceholder">
    <form id="form1" runat="server">

        <label id="customer">Selcet a Customer to add :</label>
        <asp:DropDownList ID="ddlCustomer" CssClass="info" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="CustomerID"></asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DigitalManagerConnectionString %>" ProviderName="<%$ ConnectionStrings:DigitalManagerConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Customer]"></asp:SqlDataSource>
        <label id="customerID">Customer's ID :</label>
        <asp:Label ID="lblCustomerID" runat="server" Text="ID" CssClass="dynamicLabel"></asp:Label>
        <label id="address">Address :</label>
        <asp:Label ID="lblAddress" runat="server" Text="ADDR" CssClass="dynamicLabel"></asp:Label>
        <label id="ctiy">City :</label>
        <asp:Label ID="lblCity" runat="server" Text="CTY" CssClass="dynamicLabel"></asp:Label>
        <label id="state">State :</label>
        <asp:Label ID="lblState" runat="server" Text="ST" CssClass="dynamicLabel"></asp:Label>
        <label id="zipCode">Zip Code :</label>
        <asp:Label ID="lblZipCode" runat="server" Text="ZIP" CssClass="dynamicLabel"></asp:Label>
        <label id="phone">Phone Number :</label>
        <asp:Label ID="lblPhone" runat="server" Text="TEL" CssClass="dynamicLabel"></asp:Label>
        <label id="email">E-mail :</label>
        <asp:Label ID="lblEmail" runat="server" Text="EMAIL" CssClass="dynamicLabel"></asp:Label>


        <asp:Button ID="btnAddCustomer" runat="server" Text="Add Customer" CssClass="button" OnClick="btnAddContact_Click"></asp:Button>
        <asp:Button ID="btnViewCustomers" runat="server" Text="View Customer List" CssClass="button" OnClick="btnViewContacts_Click"></asp:Button>
                    
        <asp:Label runat="server" ID="lblErrorMessage" Text="" CssClass="error"></asp:Label>
    </form>
</asp:Content>
  
    
                

                